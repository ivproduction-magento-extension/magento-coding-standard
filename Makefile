PHP = php

all: cs

ifdef CHANGED
DIFF_REF = HEAD
CS_FILES_ = $(shell git diff --name-only $(DIFF_REF) | grep -E 'app/code/' | xargs -I '{}' realpath --relative-to=. $(shell git rev-parse --show-toplevel)/'{}' 2> /dev/null | grep -v '\.\.' | while read f; do test -e $$f && echo $$f; done)
CS_FILTER = %.php %.phtml
CS_FILES = $(filter $(CS_FILTER),$(CS_FILES_))
else ifdef CUSTOM_PATH
CS_FILES = $(CUSTOM_PATH)
else
CS_FILES = /var/www/html/app/code
endif

CS_TOOLS = phpcs phpmd phpstan

phpcs:
	../../bin/phpcs --standard=Magento2 --report=emacs $(CS_FILES) --standard=phpcs-ruleset.xml

phpcbf:
	../../bin/phpcbf $(CS_FILES)

phpstan:
	../../bin/phpstan analyse $(CS_FILES) --level=2

phpmd:
	../../bin/phpmd $(CS_FILES) text phpmd-ruleset.xml

cs:
	retval=0; for i in $(CS_TOOLS); do \
		echo ----------------------- $$i -------------------------; \
		$(MAKE) $$i || retval=1; \
	done; exit $$retval

